
import java.io.*;

class Program5 {
	
	public static void main(String[] args)throws Exception{
		
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	System.out.print("Input: ");

	int x = Integer.parseInt(br.readLine());	
	
	System.out.print("Output: ");
	
	System.out.println("The series of all perfect number from 1 to " + x + " is ");

	for (int i = 1 ; i <= x ; i++){
		
		if (i % 2 == 0){
			
			int sum = 0;

			for (int j = 1 ; j <= i / 2 ; j++){
				
				if (i % j == 0)
					sum = sum + j;
				else
					continue;
			}

			if (sum == i)
				System.out.print(i + " ");
			else
				continue;
		}
		else
			continue;
	}
	System.out.println();
	}
}
