
import java.io.*;

class Program1 {
	
	public static void main(String[] args)throws Exception{
		
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	System.out.print("Input: ");

	int x = Integer.parseInt(br.readLine());	
	
	System.out.print("Output: ");
	
	int y = 1;

	for (int i = 1 ; i <= 4 ; i++){
		
		System.out.print(y + " ");
		y = y + x;
	}
	System.out.print("....");
	System.out.println();

	}
}
